export { WaxContext, useInjection } from './src/WaxContext';
export { default as ComponentPlugin } from './src/ComponentPlugin';
export { default as Wax } from './src/Wax';
