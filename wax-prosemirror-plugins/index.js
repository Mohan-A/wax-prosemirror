export { default as TrackChangePlugin } from './src/trackChanges/TrackChangePlugin';

export { default as CommentPlugin } from './src/comments/CommentPlugin';
export { default as WaxSelectionPlugin } from './src/WaxSelectionPlugin';
export { default as highlightPlugin } from './src/highlightPlugin';

export { default as mathPlugin } from './src/math/math-plugin';
export { default as mathSelectPlugin } from './src/math/math-select';
export { default as FindAndReplacePlugin } from './src/findAndReplace/FindAndReplacePlugin';
